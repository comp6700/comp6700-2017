import javafx.animation.*;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.geometry.VPos;
import javafx.scene.Group;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.input.KeyCode;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javafx.util.Duration;
import model.Feeder;
import model.Message;

import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.*;

import static java.util.Arrays.asList;
import static model.Message.Topic;

/**
 * Created with IntelliJ IDEA.
 * User: abx
 * Date: 15/4/17
 * Time: 1:20 AM
 * Main class for Ass2 application "Courant-En-Tete" --
 * running headlines. JavaFX Application which
 * should set up a simple scene with a menu bar for file
 * operations, and some controls to operate the running
 * messages -- to pause and resume, speed-up and slow-down,
 * skip to next message, or rewind to previous. Also, a row
 * of control check boxes can be added to control message
 * topic selections to be displayed in the running headlines.
 * One more extension is to implement a priority queue for
 * the message buffer (@see model.Feeder)
 */


public class RunningHeadline extends Application {

    // dimensions of the scene with text
	private final double displayWidth = 750;
    private final double displayHeight = 120;
    private double playSpeed = 0.2; // make larger to play faster
    private Group group; // container for controls and text
    private Feeder feeder; // model class which stores and processes messages
    private List<Text> newsPieces; // may be used to operate on the messages
    private List<Timeline> timelines = new ArrayList<>(); // animated messages
    private boolean running;

    
	/**
	 * removes any messages from the news buffer and fills it
	 * with news one which are read from a file
	 * @param newsFileName the name of file to open
	 */
	private void setUpFeed(String newsFileName) throws IOException {
            feeder.getBuffer().clear();
            feeder.fillNewsBuffer(Paths.get(newsFileName));
    }


    /**
     * given the contents of the message buffer, creates a list
     * of timeline objects with start and end properties for text
     * objects set up, the speed at which each message timeline will
     * be palyed (the same for all -- the messages are like railroad cars
     * in one train chain)
     */
	private void setUpTimeline() {
        double offset = displayWidth + 20; // starting x-pos for message stream
        double playtime = 0;
        Queue<Message> messageBuffer = feeder.getBuffer();
        while (!messageBuffer.isEmpty()) {
            Timeline timeline = new Timeline();
            timelines.add(timeline);
            Message latestMessage;
            latestMessage = messageBuffer.poll();
            String messageBody = latestMessage.text + " ";
            Text text = new Text(offset, displayHeight - 15,
                    messageBody);
            // use another font if this one is not available on your system
            text.setFont(Font.font("Tahoma", FontWeight.BLACK, 80));
            text.setTextOrigin(VPos.BASELINE);
            double mesWidth = text.getLayoutBounds().getWidth();
            playtime += mesWidth / playSpeed;
            setNewsPieceForRun(text, group, timeline, playtime);
            offset += mesWidth;
        }
    }

    /**
     * defines parameters of (message) timeline
     * @param text is a text object which will be animated
     * @param groups is the parent container to which the text is added as child
     * @param tl is a timeline for individual animation of this text
     * @param playtime is the time for tl to be played (no autoreverse, 1 cycle)
     * 
     * tl is defined to remove itself from the timelines list when it is 
     * finished (this may need to be removed for features like "play previous"
     * to be implemented); the reason for this removal is to reduce the memory
     * usage mainly. Also, a diagnostic messages is added to be printed when the
     * tl timeline complets (can be removed with no harm)
     */
	private void setNewsPieceForRun(Text text, Group group, 
				Timeline tl, double playtime) {
        group.getChildren().add(text);
        int mesLength = (int) text.getLayoutBounds().getWidth();

        tl.setCycleCount(1);
        tl.setAutoReverse(false);
        KeyValue kv = new KeyValue(text.xProperty(),
                -10 - mesLength, Interpolator.LINEAR);
        KeyFrame kf = new KeyFrame(Duration.millis(playtime), kv);
        tl.getKeyFrames().add(kf);
        tl.setOnFinished(e -> {
            timelines.remove(tl); // THINK HARD IF YOU NEED TO HAVE HIS!
            System.out.printf("The end of the message line is at %.2f%n",
                    text.getLayoutBounds().getMaxX());
        });
        timelines.add(tl);
    }
	
    /**
     * start (resumes) running all (remianing) timeline messages
     */
	private void runNewsStream() {
        timelines.forEach(Animation::play);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        Parent root = FXMLLoader.load(getClass().getResource("view/display.fxml"));
        primaryStage.setTitle("News just in...");
        // next two lines are needed if you read a command-line arguments
        Parameters params = getParameters();
        String newsMockFile = params.getRaw().get(0);
        
		// messages are provisoned from the data file and placed into buffer
		feeder = new Feeder();
        feeder.fillNewsBuffer(Paths.get(newsMockFile));
		
        // container which will host the texts (also menu and selection 
        // controls)
		group = new Group(); 
		// the scene itself
        Scene scene = new Scene(group, displayWidth, displayHeight);
        scene.getStylesheets().add("view/styles.css");

        // examples of call-backs to control the timeline
        // via key-board; can be removed when menu and proper controls
        // like check-boxes are added
		scene.onKeyPressedProperty().set(ke -> {
            if (ke.isMetaDown() && ke.getCode() == KeyCode.Q)
                Platform.exit();
            else if (ke.getCode() == KeyCode.P)
                timelines.forEach(tl -> tl.pause());
            else if (ke.getCode() == KeyCode.R)
                timelines.forEach(tl -> tl.play());
        });

        // initialise all timelines for text messages and run them
		setUpTimeline();
		runNewsStream();
        
		primaryStage.setResizable(false);
        primaryStage.setScene(scene);
        primaryStage.show();
        primaryStage.setOnCloseRequest(e -> Platform.exit());

    }

    public static void main(String[] args) throws IOException {
        launch(args);
    }
}
