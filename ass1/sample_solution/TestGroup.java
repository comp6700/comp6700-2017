import java.time.ZonedDateTime;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class TestGroup {

    @Test
    public void testContainsTweet() {
        Tweet t = new Tweet();
        t.coordinates = new Coordinates(1.0, 2.0);
        t.timestamp = ZonedDateTime.parse("2017-03-06T07:00:00+10:00");
        Group g = new Group("dummy", t, true);
        Tweet t2 = new Tweet();
        t2.coordinates = new Coordinates(0.8, 2.2);
        t2.timestamp = ZonedDateTime.parse("2017-03-06T10:00:00+10:00");
        g.add(t2, true);
        assert g.contains(t);
        assert g.contains(t2);
    }

    @Test
    public void testCloseEnoughTweet() {
        Tweet t = new Tweet();
        t.coordinates = new Coordinates(1.0, 2.0);
        t.timestamp = ZonedDateTime.parse("2017-03-06T07:00:00+10:00");
        Group g = new Group("dummy", t, true);
        assert g.closeEnough(t);

        Tweet t2 = new Tweet();
        t2.coordinates = new Coordinates(0.8, 2.2);
        t2.timestamp = ZonedDateTime.parse("2017-03-06T10:00:00+10:00");
        assert g.closeEnough(t2);
    }

    @Test
    public void testCloseEnoughGroup() {
        Tweet t = new Tweet();
        t.coordinates = new Coordinates(1.0, 2.0);
        t.timestamp = ZonedDateTime.parse("2017-03-06T07:00:00+10:00");
        Group g = new Group("dummy", t, true);
        assert g.closeEnough(t);
        Tweet t2 = new Tweet();
        t2.coordinates = new Coordinates(1.1, 1.8);
        t2.timestamp = ZonedDateTime.parse("2017-03-06T07:00:00+10:00");
        assert g.closeEnough(t2);
        g.add(t2, true);

        Tweet t3 = new Tweet();
        t3.coordinates = new Coordinates(0.8, 2.2);
        t3.timestamp = ZonedDateTime.parse("2017-03-06T10:00:00+10:00");
        Group g2 = new Group("dummy", t3, true);
        assert g2.closeEnough(t3);

        assert g.closeEnough(g2);
        assert g2.closeEnough(g);

        Tweet t4 = new Tweet();
        t4.coordinates = new Coordinates(0.6, 2.4);
        t4.timestamp = ZonedDateTime.parse("2017-03-06T13:00:00+10:00");
        assert g2.closeEnough(t4);
        g2.add(t4, true);
        assert g.closeEnough(g2);
        assert g2.closeEnough(g);

        // combine both groups
        g2.add(g);
        assert g2.contains(t);
        assert g2.contains(t2);
        assert g2.contains(t3);
        assert g2.contains(t4);
    }

    /**
     * Three groups of a single tweet, where the end groups are far from each
     * other but close to the middle group.  Can we combine them?
     */
    @Test
    public void testStringOfPearls() {
        Tweet t = new Tweet();
        t.coordinates = new Coordinates(1.0, 2.0);
        t.timestamp = ZonedDateTime.parse("2017-03-06T07:00:00+10:00");
        Group g = new Group("dummy", t, true);
        assert g.contains(t);

        Tweet t2 = new Tweet();
        t2.coordinates = new Coordinates(1.9, 2.9);
        t2.timestamp = ZonedDateTime.parse("2017-03-06T17:00:00+10:00");
        assert !g.closeEnough(t2);
        Group g2 = new Group("dummy", t2, true);
        assert g2.contains(t2);
        assert !g.closeEnough(g2);

        Tweet t3 = new Tweet();
        t3.coordinates = new Coordinates(1.45, 2.45);
        t3.timestamp = ZonedDateTime.parse("2017-03-06T12:00:00+10:00");
        assert g.closeEnough(t3);
        assert g2.closeEnough(t3);
        Group g3 = new Group("dummy", t3, true);
        assert g3.contains(t3);
        assert g.closeEnough(g3);
        assert g3.closeEnough(g);
        assert g2.closeEnough(g3);
        assert g3.closeEnough(g2);

        g.add(g3);
        assert g.closeEnough(g2);
        assert g2.closeEnough(g);
        g.add(g2);
        assert g.contains(t);
        assert g.contains(t2);
        assert g.contains(t3);
    }
}
