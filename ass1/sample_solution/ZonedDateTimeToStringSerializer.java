import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

import java.io.IOException;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

/**
 * A class to convert a ZonedDateTime object to Twitter's JSON timestamp string format.
 * This functionality is provided by JSR-310, but the students aren't allowed to use it.
 * I.e. annotate the ZonedDateTime field with:
 *
 *      @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = Tweet.TIMESTAMP_FORMAT)
 *
 * register the JavaTimeModule:
 *
 *      mapper.registerModule(new JavaTimeModule());
 *
 * and add the jackson-datatype-jsr310 JAR to the classpath.
 */
public class ZonedDateTimeToStringSerializer extends JsonSerializer<ZonedDateTime> {

    @Override
    public void serialize(ZonedDateTime timestamp,
                          JsonGenerator jsonGenerator,
                          SerializerProvider serializerProvider)
            throws IOException, JsonProcessingException {
        jsonGenerator.writeObject(DateTimeFormatter.ofPattern(Tweet.TIMESTAMP_FORMAT).format(timestamp));
    }

    @Override
    public Class<ZonedDateTime> handledType() {
        return ZonedDateTime.class;
    }
}
