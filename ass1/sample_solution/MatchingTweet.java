/**
 * Represents a match from a Tweet to a given topic.
 */
public class MatchingTweet {

    public static enum MatchType { NO_MATCH, DEFINITIVE, TENTATIVE};

    public MatchType type;
    public Tweet tweet;

    public MatchingTweet(Tweet tweet, MatchType type) {
        this.tweet = tweet;
        this.type = type;
    }
}
