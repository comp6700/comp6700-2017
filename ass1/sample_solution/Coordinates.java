public class Coordinates {
    public final double longitude;
    public final double latitude;

    public Coordinates(double longitude, double latitude) {
        this.longitude = longitude;
        this.latitude = latitude;
    }

    public String toString() {
        return "[" + longitude + ", " + latitude + "]";
    }
}
