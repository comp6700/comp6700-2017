import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Matches Tweets against topics by counting the number of topic keywords found in the text of each tweet.
 */
public class TopicMatcher {

    /**
     * The text of a tweet is matched against the keywords from each topic set.
     * If the text is found to contain the keywords, and some of those keywords are also present in other topic sets,
     * the text is also matched against all keywords which are present in those topic sets. For example, if a tweet
     * contains the keyword "fire", it should be matched against all keywords in the Topic 1, but also against all
     * keywords from Topic 2 and Topic 3, since finding "fire" in the tweet may, in fact, be indicative of the topic
     * "pop-music-event" (represented by the Topic 2 set), or of the topic "political-debate" (represented by the
     * Topic 3 set).
     * <p>
     * Rules:
     * tentatively accept a tweet as reporting the topic of interest if it contains only one keyword which is present in
     * the corresponding topic set and also in one or more other sets (e.g. "fire" which is present in
     * "natural_disaster", "pop-music-event" and "political-debate" topic sets);
     * otherwise, we will remove all keywords which are shared by the topic of interest and any other topic set
     * (e.g. "fire" will be removed since it is shared between "natural_disaster", "pop-music-event" and
     * "political-debate"), and compute a difference between the number of remaining keyword which belong to the chosen
     * set and all other keywords from different sets;
     * - if the difference is positive, accept the tweet as reporting,
     * - if the difference is negative, reject the tweet,
     * - if the difference is zero, accept the tweet tentatively
     * When computing the difference, a keyword that is not present in the topic of interest but is present in multiple
     * other topic sets is only counted once.
     *
     * @param topics the complete list of topics
     * @param focusTopic the topic to focus on; must be a member of topic list
     * @param tweets the list of tweets to match against topics
     * @return a list of matching tweets for the topic of interest
     */
    public static List<MatchingTweet> getMatchingTweets(List<Topic> topics, Topic focusTopic, List<Tweet> tweets) {
        List<MatchingTweet> matchingTweets = new ArrayList<>();
        List<String> focusKeywords = focusTopic.keywords;
        Set<String> otherKeywords = new HashSet<>();
        for (Topic t : topics) {
            if (t != focusTopic) {
                otherKeywords.addAll(t.keywords);
            }
        }
        for (Tweet tweet : tweets) {
            //System.out.println("comparing tweet text " + tweet);
            MatchingTweet.MatchType match = MatchingTweet.MatchType.NO_MATCH;
            int exclusive = 0;
            int shared = 0;
            int other = 0;
            final String text = tweet.text.toLowerCase();
            for (String keyword : focusKeywords) {
                if (text.contains(keyword)) {
                    if (otherKeywords.contains(keyword)) {
                        //System.out.println("found shared keyword " + keyword);
                        shared++;
                    } else {
                        //System.out.println("found exclusive keyword " + keyword);
                        exclusive++;
                    }
                }
            }
            if (shared == 0) {
                if (exclusive > 0) {
                    //System.out.println("definitive match");
                    match = MatchingTweet.MatchType.DEFINITIVE;
                } else {
                    match = MatchingTweet.MatchType.NO_MATCH;
                }
            } else {
                // count keywords in other topics
                for (String keyword : otherKeywords) {
                    if (text.contains(keyword) && !focusKeywords.contains(keyword)) {
                        //System.out.println("found other keyword " + keyword);
                        other++;
                    }
                }
                //System.out.println("count = " + exclusive + "-"+other+"="+(exclusive-other));
                if (exclusive > other) {
                    //System.out.println("definitive match");
                    match = MatchingTweet.MatchType.DEFINITIVE;
                } else if (exclusive == other) {
                    //System.out.println("tentative match");
                    match = MatchingTweet.MatchType.TENTATIVE;
                } else {
                    match = MatchingTweet.MatchType.NO_MATCH;
                }
            }
            if (match == MatchingTweet.MatchType.DEFINITIVE || match == MatchingTweet.MatchType.TENTATIVE) {
                matchingTweets.add(new MatchingTweet(tweet, match));
            }
        }
        return matchingTweets;
    }
}
