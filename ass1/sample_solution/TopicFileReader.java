import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

public class TopicFileReader {
    public static List<Topic> readTopics() throws IOException {
        ArrayList<Topic> topics = new ArrayList();
        Path path = Paths.get("topics.properties");
        Properties props = new Properties();
        props.load(Files.newInputStream(path));

        props.forEach((k, v) -> {
            Topic t = new Topic((String) (k));
            try {
                Files.lines(Paths.get(String.format("%s", v)))
                        .forEach(s -> {
                            if (!s.isEmpty()) t.keywords.add(s.toLowerCase());
                        });
            } catch (IOException ioe) {
                System.err.printf("The keyword set %s cannot be opened%s", ioe);
                // TODO throw exception instead of exiting
                System.exit(1);
            }
            topics.add(t);
        });
        return topics;
    }
}
