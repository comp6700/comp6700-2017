import java.util.ArrayList;
import java.util.List;

/**
 * Represents a topic, which comprises a set of keywords that may be matched to the text of Tweets.
 */
public class Topic {
    String name;
    List<String> keywords = new ArrayList<>();

    public Topic(String name) {
        this.name = name;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("topic: ").append(name).append("\n");
        for(String keyword:keywords) {
            sb.append(keyword).append("\n");
        }
        return sb.toString();
    }
}
