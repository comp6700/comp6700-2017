import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.util.*;

/**
 * This program analyses a list of tweets, matching the text of each tweet against keywords from a list of topics
 * stored in a file topics.properties.
 * Matching tweets are combined into groups according to proximity in time and space.
 * After allowing the user to choose a topic of interest, the program reports both matching tweets and any groups that
 * are found for that topic.
 * <p>
 * Usage: java TwitterAnalyser tweets.json
 */
public class TwitterAnalyser {
    public final static int SELECTION_ALL_TOPICS = 0;

    public static void main(String[] args) {
        if (args.length < 1) {
            System.err.println("Usage: java TwitterAnalyser <filename>");
            System.exit(1);
        }

        List<Topic> topics = null;
        try {
            topics = TopicFileReader.readTopics();
        } catch (IOException e) {
            System.err.println("Error reading topics file " + args[0]);
            e.printStackTrace();
            System.exit(1);
        }

        int numMenuItems = printTopicMenu(topics);
        int selection = getUserSelection(topics, numMenuItems);

        // read tweets
        List<Tweet> tweets = null;
        try {
            tweets = TweetFileReader.readTweetFile(args[0]);
        } catch (IOException e) {
            System.err.println("Error reading tweets file " + args[0]);
            e.printStackTrace();
            System.exit(1);
        }

        List<Group> groups = findGroupsForTopics(topics, selection, tweets);
        printGroups(groups);
    }

    /**
     * Given a list of Tweets, find the matching tweets for a the topic of interest and combine into groups.
     */
    private static List<Group> findGroupsForTopics(List<Topic> topics, int selection, List<Tweet> tweets) {
        List<Group> groups = new ArrayList<>();
        for (int i = 0; i < topics.size(); i++) {
            if (selection == SELECTION_ALL_TOPICS || selection == (i + 1)) {
                Topic topic = topics.get(i);
                List<MatchingTweet> matchingTweets = TopicMatcher.getMatchingTweets(topics, topic, tweets);

                System.out.println(matchingTweets.size() + " tweets matching topic " + topic.name);
                for (MatchingTweet mt : matchingTweets) {
                    System.out.println(mt.tweet);
                }

                groupTweets(groups, topic, matchingTweets);
            }
        }

        combineGroups(groups);

        groups.removeIf(g -> g.numberOfDefinitives == 0);

        // for debugging only - check if any groups are still left that are close to each other but have not been merged
        for (int i = 0; i < groups.size(); i++) {
            for (int j = i + 1; j < groups.size(); j++) {
                if (groups.get(i).closeEnough(groups.get(j))) {
                    System.err.println("found close groups: "
                            + i + " " + j + " " + groups.get(i) + " " + groups.get(j));
                }
            }
        }

        return groups;
    }

    private static void groupTweets(List<Group> groups, Topic topic, List<MatchingTweet> matchingTweets) {
        for (MatchingTweet mt : matchingTweets) {
            if (mt.tweet.coordinates != null) {
                boolean definitive = (mt.type == MatchingTweet.MatchType.DEFINITIVE);
                boolean found = false;
                for (Group g : groups) {
                    if (g.topicType.equals(topic.name) && g.closeEnough(mt.tweet)) {
                        g.add(mt.tweet, definitive);
                        found = true;
                        break;
                    }
                }
                if (!found) {
                    Group g = new Group(topic.name, mt.tweet, definitive);
                    groups.add(g);
                }
            }
        }
    }

    /**
     * Given a list of groups, combine together all groups that refer to the same topic and are close enough together
     * in space and time.
     *
     * @param groups a list of Groups
     * @return a new list of Groups where all close groups have been combined
     */
    private static void combineGroups(List<Group> groups) {
        int numGroups = groups.size();
        Group[] groupArray = groups.toArray(new Group[numGroups]);

        for (int i = 0; i < numGroups; i++) {
            Group g = groupArray[i];
            boolean combined = false;
            do {
                combined = false;
                for (int j = i + 1; j < numGroups; j++) {
                    Group other = groupArray[j];
                    if (g.topicType.equals(other.topicType) && g.closeEnough(other)) {
                        // merge groups: add group j to group i, and then remove group j
                        g.add(other);
                        for (int k = j + 1; k < numGroups; k++) {
                            groupArray[k - 1] = groupArray[k];
                        }
                        combined = true;
                        numGroups--;
                        j--;
                    }

                }
            } while (combined); // group i was expanded.  check it against all other groups, again
        }

        groups.clear();
        groups.addAll(Arrays.asList(groupArray));
    }

    private static void printGroups(List<Group> groups) {
        System.out.println("found " + groups.size() + " groups");
        ObjectMapper mapper = new ObjectMapper();
        int totalDefinitive = 0;
        int totalTentative = 0;
        for (Group g : groups) {
            try {
                totalDefinitive += g.numberOfDefinitives;
                totalTentative += g.numberOfTentatives;
                String jsonString = mapper.writeValueAsString(g);
                System.out.println(jsonString);
            } catch (JsonProcessingException e) {
                e.printStackTrace();
            }
        }
        System.out.println("definitive: " + totalDefinitive + " tentative: " + totalTentative);
    }

    private static int printTopicMenu(List<Topic> topics) {
        System.out.println("The following topics can be used in search:");
        int menuItem = 0;
        System.out.printf("%d. all topics\n", menuItem++);
        for (Topic topic : topics) {
            System.out.printf("%d. %s\n", menuItem++, topic.name);
        }
        System.out.println("Choose by number (empty to quit):");
        return menuItem;
    }

    private static int getUserSelection(List<Topic> topics, int numMenuItems) {
        Scanner in = new Scanner(System.in);
        int selection = -1;
        try {
            selection = in.nextInt();
            if (selection < 0 || selection >= numMenuItems) {
                System.err.println("Error: topic choice must be in the range 0.." + (numMenuItems - 1));
                System.exit(1);
            }

            Topic focus = null;
            if (selection > 0) {
                focus = topics.get(selection - 1);
                System.out.println("User chose " + focus.name);
            } else {
                System.out.println("User chose all topics");
            }
            return selection;

        } catch (InputMismatchException e) {
            System.err.println("Error: user did not input a number");
            System.exit(1);
        } catch (NoSuchElementException e) {
            System.err.println("Error: user did not select a topic");
            System.exit(1);
        }
        return selection;
    }
}
