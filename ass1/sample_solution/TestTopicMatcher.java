import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class TestTopicMatcher {

    static ArrayList<Topic> topics = new ArrayList<>();

    @BeforeAll
    public static void initAll() {
        Topic fire = new Topic("fire emergency");
        fire.keywords.add("fire");
        fire.keywords.add("bush");
        fire.keywords.add("smoke");
        topics.add(fire);
        Topic flood = new Topic("flood emergency");
        flood.keywords.add("flood");
        flood.keywords.add("rain");
        flood.keywords.add("river");
        topics.add(flood);
        Topic hiking = new Topic("hiking");
        hiking.keywords.add("fire");
        hiking.keywords.add("backpack");
        hiking.keywords.add("bush");
        hiking.keywords.add("river");
        topics.add(hiking);
    }

    @Test
    public void testDefinitiveMatch() {
        Tweet t = new Tweet();
        t.text = "I've seen fire and I've seen rain";
        ArrayList<Tweet> tweets = new ArrayList<>();
        tweets.add(t);
        List<MatchingTweet> matches = TopicMatcher.getMatchingTweets(topics, topics.get(0), tweets); // fire
        assertEquals(matches.size(), 0);

        matches = TopicMatcher.getMatchingTweets(topics, topics.get(1), tweets); // flood
        assertEquals(matches.size(), 1);
        MatchingTweet match = matches.get(0);
        assertEquals(match.type, MatchingTweet.MatchType.DEFINITIVE);

        matches = TopicMatcher.getMatchingTweets(topics, topics.get(2), tweets); // hiking
        assertEquals(matches.size(), 0);
    }

    @Test
    public void testTentativeMatch() {
        Tweet t = new Tweet();
        t.text = "Ready, Aim, Fire!";
        ArrayList<Tweet> tweets = new ArrayList<>();
        tweets.add(t);
        List<MatchingTweet> matches = TopicMatcher.getMatchingTweets(topics, topics.get(0), tweets);
        assertEquals(matches.size(), 1);
        MatchingTweet match = matches.get(0);
        assertEquals(match.type, MatchingTweet.MatchType.TENTATIVE);

        matches = TopicMatcher.getMatchingTweets(topics, topics.get(1), tweets); // flood
        assertEquals(matches.size(), 0);

        matches = TopicMatcher.getMatchingTweets(topics, topics.get(2), tweets); // hiking
        assertEquals(matches.size(), 1);
        match = matches.get(0);
        assertEquals(match.type, MatchingTweet.MatchType.TENTATIVE);
    }

    @Test
    public void testRejectAll() {
        Tweet t = new Tweet();
        t.text = "This tweet is irrelevant";
        ArrayList<Tweet> tweets = new ArrayList<>();
        tweets.add(t);

        List<MatchingTweet> matches = TopicMatcher.getMatchingTweets(topics, topics.get(0), tweets); // fire
        assertEquals(matches.size(), 0);
        matches = TopicMatcher.getMatchingTweets(topics, topics.get(1), tweets); // flood
        assertEquals(matches.size(), 0);
        matches = TopicMatcher.getMatchingTweets(topics, topics.get(2), tweets); // hiking
        assertEquals(matches.size(), 0);
    }
}
