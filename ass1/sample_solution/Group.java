import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

/**
 * Represents a group of Tweets, which are located close together in space and time.
 * Any tweets with the same topic that are separated by an interval of less than six hours and less than half a degree
 * of both latitude and longitude are assumed to belong to the same Group.
 */
public class Group {
    public static class BoundingBox {
        public final boolean found = true;
        public double topLeft[] = new double[2];
        public double bottomRight[] = new double[2];

        public String toString() {
            return "topLeft [" + topLeft[0] + "," + topLeft[1] + "], bottomRight [" + bottomRight[0] + "," + bottomRight[1] + "]";
        }
    }

    public static final double MAX_DIFF_LONGITUDE = 0.5;
    public static final double MAX_DIFF_LATITUDE = 0.5;
    public static final long MAX_DIFF_HOURS = 6;
    public String topicType;
    public int numberOfDefinitives = 0;
    public int numberOfTentatives = 0;
    public BoundingBox cluster = new BoundingBox();
    @JsonSerialize(using = ZonedDateTimeToStringSerializer.class)
    public ZonedDateTime begin;
    @JsonSerialize(using = ZonedDateTimeToStringSerializer.class)
    public ZonedDateTime end;

    public Group(String topicType, Tweet tweet, boolean definitive) {
        this.topicType = topicType;
        this.cluster.topLeft[0] = tweet.coordinates.longitude;
        this.cluster.bottomRight[0] = tweet.coordinates.longitude;
        this.cluster.topLeft[1] = tweet.coordinates.latitude;
        this.cluster.bottomRight[1] = tweet.coordinates.latitude;
        this.begin = tweet.timestamp;
        this.end = tweet.timestamp;
        if (definitive) numberOfDefinitives = 1;
        else numberOfTentatives = 1;
    }

    /**
     * Returns true if this Group contains the given Tweet, i.e. if the tweet coordinates are within the bounding box
     * of this Group.
     * @param t a Tweet
     * @return true if this group contains the given Tweet
     */
    public boolean contains(Tweet t) {
        if (t.coordinates != null) {
            double longitude = t.coordinates.longitude;
            double latitude = t.coordinates.latitude;
            return (longitude >= this.cluster.topLeft[0]
                    && longitude <= this.cluster.bottomRight[0]
                    && latitude <= this.cluster.topLeft[1]
                    && latitude >= this.cluster.bottomRight[1]
                    && !t.timestamp.isBefore(this.begin)
                    && !t.timestamp.isAfter(this.end));
        } else {
            return true;
        }
    }

    /**
     * Returns true if this group *should* contain the given Tweet, i.e. if the tweet is separated from the bounding
     * box of this group by less than half a degree of latitude/longitude in space, and less than six hours in time.
     * @param t a Tweet
     * @return true if this group should contain the given Tweet
     */
    public boolean closeEnough(Tweet t) {
        if (t.coordinates != null) {
            double longitude = t.coordinates.longitude;
            double latitude = t.coordinates.latitude;
            return (longitude > this.cluster.topLeft[0] - MAX_DIFF_LONGITUDE
                    && longitude < this.cluster.bottomRight[0] + MAX_DIFF_LONGITUDE
                    && latitude < this.cluster.topLeft[1] + MAX_DIFF_LATITUDE
                    && latitude > this.cluster.bottomRight[1] - MAX_DIFF_LATITUDE)
                    && t.timestamp.isAfter(this.begin.minusHours(MAX_DIFF_HOURS))
                    && t.timestamp.isBefore(this.end.plusHours(MAX_DIFF_HOURS));
        } else {
            return false;
        }
    }

    /**
     * Returns true if this Group is close enough to another Group that the two groups should be combined, i.e.
     * if the bounding boxes of the two groups are separated by less than half a degree of latitude/longitude in space,
     * and less than six hours in time.
     * @param other a Group
     * @return true if this group is close enough to the other Group that the two groups should be combined
     */
    public boolean closeEnough(Group other) {
        //System.out.println("comparing group " + this + " with " + g);
        return (this.cluster.topLeft[0] - MAX_DIFF_LONGITUDE < other.cluster.bottomRight[0]
                && this.cluster.bottomRight[0] + MAX_DIFF_LONGITUDE > other.cluster.topLeft[0]
                && this.cluster.topLeft[1] + MAX_DIFF_LATITUDE > other.cluster.bottomRight[1]
                && this.cluster.bottomRight[1] - MAX_DIFF_LATITUDE < other.cluster.topLeft[1])
                && this.begin.minusHours(MAX_DIFF_HOURS).isBefore(other.end)
                && this.end.plusHours(MAX_DIFF_HOURS).isAfter(other.begin);
    }

    /**
     * Add the given Tweet to this Group, and extend the bounding box if necessary.
     * @param t a Tweet
     * @param definitive whether the given Tweet was a definitive match for the chosen topic
     */
    public void add(Tweet t, boolean definitive) {
        extendBoundingBox(t);
        if (definitive) numberOfDefinitives++;
        else numberOfTentatives++;
    }

    /**
     * Combine this group with another group, and extend the bounding box if necessary.
     * @param g another Group
     */
    public void add(Group g) {
        extendBoundingBox(g);
        this.numberOfDefinitives += g.numberOfDefinitives;
        this.numberOfTentatives += g.numberOfTentatives;
    }

    private void extendBoundingBox(Tweet tweet) {
        this.cluster.topLeft[0] = Math.min(cluster.topLeft[0], tweet.coordinates.longitude);
        this.cluster.bottomRight[0] = Math.max(cluster.bottomRight[0], tweet.coordinates.longitude);
        this.cluster.topLeft[1] = Math.max(cluster.topLeft[1], tweet.coordinates.latitude);
        this.cluster.bottomRight[1] = Math.min(cluster.bottomRight[1], tweet.coordinates.latitude);
        int earlier = tweet.timestamp.compareTo(begin);
        if (earlier < 0) begin = tweet.timestamp;
        int later = tweet.timestamp.compareTo(end);
        if (later > 0) end = tweet.timestamp;
    }

    private void extendBoundingBox(Group g) {
        this.cluster.topLeft[0] = Math.min(cluster.topLeft[0], g.cluster.topLeft[0]);
        this.cluster.bottomRight[0] = Math.max(cluster.bottomRight[0], g.cluster.bottomRight[0]);
        this.cluster.topLeft[1] = Math.max(cluster.topLeft[1], g.cluster.topLeft[1]);
        this.cluster.bottomRight[1] = Math.min(cluster.bottomRight[1], g.cluster.bottomRight[1]);
        int earlier = g.begin.compareTo(begin);
        if (earlier < 0) begin = g.begin;
        int later = g.end.compareTo(end);
        if (later > 0) end = g.end;
    }

    public String toString() {
        return topicType + " " + cluster.toString() + " "
                + DateTimeFormatter.ofPattern(Tweet.TIMESTAMP_FORMAT).format(begin) + " "
                + DateTimeFormatter.ofPattern(Tweet.TIMESTAMP_FORMAT).format(end);
    }
}
