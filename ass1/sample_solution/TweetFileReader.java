import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Stream;

public class TweetFileReader {

    /**
     * Read a list of Tweet objects from JSON records in the given Tweet file.
     */
    public static List<Tweet> readTweetFile(String filename) throws IOException {
        ArrayList<Tweet> tweets = new ArrayList();
        ObjectMapper mapper = new ObjectMapper();
        Stream<String> lines = Files.lines(Paths.get(filename),
                StandardCharsets.UTF_8);
        lines.forEach(s -> {
            try {
                Map<String,Object> tweetData = mapper.readValue(s, Map.class);
                Tweet tweet = new Tweet();
                Map<String, Object> user = (Map<String, Object>)(tweetData.get("user"));
                tweet.userId = Long.parseLong((String)user.get("id"));
                tweet.userName = (String)user.get("name");
                tweet.text = (String)(tweetData.get("text"));
                tweet.timestamp = ZonedDateTime.parse((String)(tweetData.get("created_at")),
                        DateTimeFormatter.ofPattern(Tweet.TIMESTAMP_FORMAT));
                Map<String, Object> coordinates = (Map<String, Object>)(tweetData.get("coordinates"));
                if (coordinates != null) {
                    List<Double> coordData = (List<Double>)(coordinates.get("coordinates"));
                    tweet.coordinates = new Coordinates(coordData.get(0), coordData.get(1));
                }
                tweets.add(tweet);
            } catch (IOException ioe) {
                System.out.printf("Bad json record %s%n", ioe);
            }
        });
        return tweets;
    }

}
