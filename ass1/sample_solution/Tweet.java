import com.fasterxml.jackson.annotation.JsonFormat;

import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

/**
 * Represents a single tweet in the Twitter feed.
 */
public class Tweet {
    public static final String TIMESTAMP_FORMAT = "EEE MMM dd HH:mm:ss Z yyyy";

    public long userId;
    public String userName;
    public String text;
    @JsonFormat(shape=JsonFormat.Shape.STRING, pattern=TIMESTAMP_FORMAT)
    public ZonedDateTime timestamp;
    public Coordinates coordinates;

    public String toString() {
                if (coordinates != null) {
                    return String.format("userId: \"%d\", userName: \"%s\", coordinates: \"%s\", timestamp: \"%s\"",
                            userId, userName, coordinates, DateTimeFormatter.ofPattern(TIMESTAMP_FORMAT).format(timestamp));
                } else {
                    return String.format("userId: \"%d\", userName: \"%s\", timestamp: \"%s\"",
                            userId, userName, DateTimeFormatter.ofPattern(TIMESTAMP_FORMAT).format(timestamp));
        }
    }
}
