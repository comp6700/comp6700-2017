This is a master repository to host home works, and Assignments One and Two for 
Introductory Programming (in Java) course, COMP2140/6700, in 2017.

You should fork it and add your lecturers and tutors:

- Josh Milthorpe
- Alexei Khorev
- Jason Bolito
- Debashish (Dev) Chakraborty
- Sean Cuskelly

as **reporters** to thus forked repository which you need to keep **private**. 
The reporters will be able to clone your repository for marking after you push
your work into it for submission, but they will not be able to push into it
(only you should be able to do it). Nobody else (including your fellow 
students) needs to have read or write access to it. 
The details for forking and basic *Git* usage are described in the 
[Git and GitLab](https://cs.anu.edu.au/courses/comp6700/labs/gitlab/).

Try to preserve the original repository structure: 

```bash
├── README.md
├── ass1
├── ass2
└── homeworks
    ├── hw3
    ├── hw4
    ├── hw5
    ├── hw6
    ├── hw7
    └── hw8

```

When submitting either assignment, or homework, please
keep the relevant files in the corresponding subdirectory; if
you submit a homework (starting from Homework Three, you can either present
it in class like you did at the beginning, or submit it on Friday, 6pm of 
the due week), then commit and push your code in the subdirectory of the 
homework -- *eg*, the Homework Four should be submitted inside 
``homework/hw4``. Following this rule the chance of your work going missing and 
unmarked will be reduced.
