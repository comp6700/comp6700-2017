import org.junit.jupiter.api.Test;

import java.util.Iterator;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * This class tests the implementation of the BookList interface in the BookListWithLL class.
 */
public class TestBookList {

    /**
     * Test that after adding an item to a newly created list, that item is the first item in the list and can be
     * retrieved by calling list.get(0) .
     */
    @Test
    public void testAddGet() {
        BookList list = new BookListWithLL();
        Book caterpillar = new Book("The Very Hungry Caterpillar", true);
        list.add(caterpillar);
        Book first = list.get(0);
        assertEquals(caterpillar, first);
    }

    /**
     * Test that the size of a newly created list is zero, and that when an item is added to the list, the size of the
     * list increases by one.
     */
    @Test
    public void testSize() {
        BookList list = new BookListWithLL();
        assertEquals(list.size(), 0);
        list.add(new Book("The Very Hungry Caterpillar", true));
        assertEquals(list.size(), 1);
        list.add(new Book("Java Software Solutions", false));
        assertEquals(list.size(), 2);
        list.add(new Book("A Hitchhiker's Guide to the Galaxy", true));
        assertEquals(list.size(), 3);
    }

    /**
     * Test that the addFirst method correctly inserts items at the start of the list, and that other items in the list
     * are reordered appropriately.
     */
    @Test
    public void testAddFirst() {
        BookList list = new BookListWithLL();
        list.add(new Book("Java Software Solutions", false));

        Book caterpillar = new Book("The Very Hungry Caterpillar", true);
        list.addFirst(caterpillar);
        Book first = list.get(0);
        assertEquals(caterpillar, first);

        Book hhgttg = new Book("A Hitchhiker's Guide to the Galaxy", true);
        list.addFirst(hhgttg);
        first = list.get(0);
        assertEquals(hhgttg, first);
        Book second = list.get(1);
        assertEquals(caterpillar, second);
    }

    /**
     * Test that the iterator() method returns an iterator over all the items in the list.
     */
    @Test
    public void testIterator() {
        BookList list = new BookListWithLL();
        list.add(new Book("The Very Hungry Caterpillar", true));
        list.add(new Book("Java Software Solutions", false));
        list.add(new Book("A Hitchhiker's Guide to the Galaxy", true));
        int size = list.size();
        Iterator<Book> iter = list.iterator();
        int count = 0;
        while (iter.hasNext()) {
            System.out.println(iter.next());
            count++;
        }
        assertEquals(count, size);
    }
}
