import java.util.Iterator;

public interface BookList extends Iterable<Book> {
	/** Add a new Book at the beginning of the list. */
	public void addFirst(Book newBook);

	/** Add a new Book at the end of the list. */
	public boolean add(Book newBook);

	/** If the given book is contained in the list, remove it.
	 * @param book a book to remove
	 * @return true if the book was removed
	 */
	//public boolean remove(Book book); // not yet defined

	/**
	 * Insert a new book at the given position in the list.
	 * Assumes 0 <= position < list.size
	 * @param newBook the new book to insert
	 * @param position the position in the list at which to insert
	 */
	public void insert(Book newBook, int position);

	/**
	 * @return the book at the given position in the list
	 */
	public Book get(int position);

	/**
	 * @return true if the list is empty i.e. size() == 0
	 */
	public boolean isEmpty();

	/**
	 * @return true if this list contains the given book
	 */
	//public boolean contains(Book book); // not yet here

	public int size();
	public String toString();
	public Iterator<Book> iterator();
}

