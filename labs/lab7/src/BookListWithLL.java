import java.util.Iterator;
import java.util.NoSuchElementException;

/**
 * BookList implemented as a linked list
 */
public class BookListWithLL implements BookList, Iterable<Book> {

    private BookNode list;
    private int count;

    /**
     * @return length of list
     */
    public int size() {
        return count;
    }

    /**
     * @return true if list has no elements
     */
    public boolean isEmpty() {
        return (size() < 1);
    }

    /**
     * Add a book to the beginning of the list
     *
     * @param newBook the Book to be added
     */
    public void addFirst(Book newBook) {
        BookNode node = new BookNode(newBook, null);

        if (list == null) {
            list = node;
        } else {
            node.next = list;
            list = node;
        }
        count++;
    }

    /**
     * Add a book to the end of the list
     *
     * @param newBook the Book to be added
     */
    public boolean add(Book newBook) {
        if (list == null) {
            list = new BookNode(newBook, null);
        } else {
            BookNode current = list;
            for (int i = 0; i < count - 1; i++) {
                current = current.next;
            }
            current.next = new BookNode(newBook, null);
        }
        count++;
        return true;
    }

    /**
     * Insert a book
     *
     * @param newBook the Book to be inserted
     * @position the number of list nodes before the insertion point (counting from 0)
     */
    public void insert(Book newBook, int position) {
        if (position < 0 || position >= count) {
            throw new IndexOutOfBoundsException("Invalid value for insertion position: " + position);
        }

        if (position == 0) {
            addFirst(newBook);
        } else {
            BookNode current = list;
            for (int i = 0; i < position-1; i++) {
                current = current.next;
            }
            // current now is equal to node at "position-1"

            BookNode node = new BookNode(newBook, current);
            node.next = current.next;
            current.next = node;
            count++;
        }
    }

    public Book get(int position) {
        if (position < 0 || position >= count) {
            throw new IndexOutOfBoundsException("Invalid value for get position: " + position);
        }

        BookNode current = list;
        for (int i = 0; i < position; i++) {
            current = current.next;
        }
        return current.item;
    }

    /**
     * @return an iterator over the book list
     */
    public Iterator<Book> iterator() {
        return new BookListIterator(list);
    }

    private static class BookNode {
        Book item;
        BookNode next;

        BookNode(Book item, BookNode next) {
            this.item = item;
            this.next = next;
        }
    }

    public class BookListIterator implements Iterator<Book> {
        private BookNode current; // the current position of the iterator

        /**
         * Constructor sets up link to header.
         *
         * @param header header node of list
         */
        public BookListIterator(BookNode header) {
            current = new BookNode(null, header); // current is header-1
        }

        /**
         * @return true if the current node is not the end node
         */
        public boolean hasNext() {
            if (current.next != null)
                return true;
            else return false;
        }

        /**
         * @return the next element
         */
        public Book next() {
            try {
                current = current.next;
                return current.item;
            } catch (NoSuchElementException e) {
                System.out.println(e);
                return null;
            }
        }

        /**
         * Dummy version of remove, subtle issue anyway
         */
        public void remove() throws UnsupportedOperationException {
        }
    }
}

