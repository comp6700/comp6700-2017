public class Book implements Comparable<Book> {

    private String title;
    private boolean fiction;

    public Book(String title, boolean fiction) {
        this.title = title;
        this.fiction = fiction;
    }

    public String toString() {
        return title;
    }

    public boolean isFiction() {
        return fiction;
    }

    /**
     * implements compareTo(Book) of Comparable so the list of books can be sorted
     *
     * @return int result of comparing +1, 0, -1
     */
    public int compareTo(Book b) {
        return this.title.compareTo(b.toString());
    }
}
