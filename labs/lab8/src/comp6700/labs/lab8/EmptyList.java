package comp6700.labs.lab8;

import java.util.function.Predicate;

/**
 * Created with IntelliJ IDEA.
 * User: abx
 * Date: 21/5/17
 * Time: 4:58 PM
 * Created for lazylists in package comp6700.labs.lab8
 */
public class EmptyList<T> implements MyList<T> {

    public T head() {
        throw new UnsupportedOperationException("No head in empty list");
    }

    public MyList<T> tail() {
        throw new UnsupportedOperationException("No tail in empty list");
    }

    public MyList<T> filter(Predicate<T> p) {
        throw new UnsupportedOperationException("Nothing to filter in empty list");
    }
}
