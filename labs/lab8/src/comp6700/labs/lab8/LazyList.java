package comp6700.labs.lab8;

import java.util.function.Predicate;
import java.util.function.Supplier;

/**
 * Created with IntelliJ IDEA.
 * User: abx
 * Date: 21/5/17
 * Time: 5:01 PM
 * Created for lazylists in package comp6700.labs.lab8
 *
 * @param <T> the type parameter
 */
public class LazyList<T> implements MyList<T> {

    final T head;
    final Supplier<MyList<T>> tail;

    public LazyList(T head, Supplier<MyList<T>> tail) {
        this.head = head;
        this.tail = tail;
    }

    /**
     * A factory method which create a LazyList of integers
     * starting from natural number n and increasing monotonously
     *
     * @param n the n
     * @return the lazy list
     */
    public static LazyList<Integer> generateFrom(int n) {
        return new LazyList<>(n, () -> generateFrom(n + 1));
    }

    public static void main(String[] args) {
        MyList<Integer> numbers = generateFrom(2);
        if (args.length < 1) {
            System.out.println("Usage: java Factorise integer");
            System.exit(1);
        }
        try {
            int limit = Integer.parseInt(args[0]);
            int i = 0;
            while (!numbers.tail().isEmpty() && i < limit) {
                System.out.printf("%d ", numbers.head());
                numbers = numbers.tail();
                i++;
            }
            System.out.println();
            System.out.println("After filtering on even numbers:");
            numbers = numbers.filter(n -> n%2 == 0);
            i = 0;
            while (!numbers.tail().isEmpty() && i < limit) {
                System.out.printf("%d ", numbers.head());
                numbers = numbers.tail();
                i++;
            }
            System.out.println();
            System.out.println("As you can see, the numbers list is still there");
        } catch (NumberFormatException nfe) {
            System.out.println("Usage: java Factorise INTEGER");
            System.exit(1);
        }
    }

    public boolean isEmpty() {
        return false;
    }

    public T head() {
        return head;
    }

    public MyList<T> tail() {
        return tail.get();
    }

    /**
     * To make up for missing filter method is an ordinary list
     * and to make LazyList be even more like streams
     */
    public MyList<T> filter(Predicate<T> p) {
        return isEmpty() ?
                this :  // new EmptyList() is also possible
                p.test(head()) ?
                        new LazyList<T>(head(), () -> tail().filter(p)) :
                        tail().filter(p);
    }

    public static MyList<Integer> primes(MyList<Integer> numbers) {
        return new LazyList<>(
                numbers.head(),
                () -> primes(
                        numbers.tail()
                                .filter(n -> n % numbers.head() != 0)
                )
        );
    }

}
