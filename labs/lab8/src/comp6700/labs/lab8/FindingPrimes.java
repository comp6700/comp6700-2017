package comp6700.labs.lab8;

import java.util.List;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import static java.util.stream.Collectors.joining;
import static java.util.stream.Collectors.toList;

public class FindingPrimes {

    public static Stream<Integer> primes(int n) {
        return IntStream.iterate(2, i -> i + 1)
                .filter(FindingPrimes::isPrime)
                .boxed()
                .limit(n); // stop at n (iterate would go to infinity)
    }

    public static boolean isPrime(int candidate) {
        int candidateRoot = (int) Math.sqrt((double) candidate);
        boolean res = IntStream.rangeClosed(2, candidateRoot)
                .noneMatch(i -> candidate % i == 0);
        return res;
    }

    private static void prettyPrint(List<Integer> list, int width) {
        int number = list.size();
        int k = width; // blockSize for printing list elements (poor-man pretty-printing)
        String block; // chunk of values for printing on one line
        System.out.println("===========================================");
        for (int l = 0; l < number / k; l++) {
            block = list.subList(l*k, l*k + k)
                    .stream()
                    .map(p -> String.format("%5d", p))
                    .collect(joining(" "));
            System.out.printf("%s%n", block);
        }
        if (number % k != 0) {
            block = list.subList(k * (number / k), number)
                    .stream()
                    .map(p -> String.format("%5d", p))
                    .collect(joining(" "));
            System.out.printf("%s%n", block);
        }
        System.out.println("===========================================");
    }

    public static void main(String[] args) {
        if (args.length < 1) {
            System.out.println("Usage: java FindingPrimes <numberOfPrimes>");
            System.exit(1);
        }
        try {
            Integer number = Integer.parseInt(args[0]);
            long start = System.currentTimeMillis();
            List<Integer> res = primes(number)
                    .collect(toList());
            long elapsed = System.currentTimeMillis() - start;
            System.out.printf("It took %.3f sec to find first %d primes%n",
                    (1.0 * elapsed) / 1_000, number);
            System.out.printf("First %d primes:%n", number);
            prettyPrint(res, 16);
            int biggestPrime = res.get(res.size() - 1);
            System.out.printf("The biggest prime: %d%n", biggestPrime);
            // "Asymptotic number of Primes" according to Chebyshev estimate
            double anp = biggestPrime / Math.log1p(biggestPrime);
            System.out.printf("The asymptotic estimate %.2f%n", anp);

        } catch (NumberFormatException nfe) {
            System.out.println("Usage: java FindingPrimes <numberOfPrimes>");
            System.exit(1);
        }
    }
}
