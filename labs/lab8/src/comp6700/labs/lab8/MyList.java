package comp6700.labs.lab8;

import java.util.function.Predicate;

/**
 * Created with IntelliJ IDEA.
 * User: abx
 * Date: 21/5/17
 * Time: 4:53 PM
 * Created for lazylists in package PACKAGE_NAME
 */
public interface MyList<T> {

    T head();

    MyList<T> tail();

    MyList<T> filter(Predicate<T> p);

    default boolean isEmpty() {
        return true;
    }

}
